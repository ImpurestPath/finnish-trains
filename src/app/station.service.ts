import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Station } from 'src/station';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class StationService {
  stations: Station[]
  constructor(private http: HttpClient) { }

  getData(url): Observable<any> {
    const address = `${API_URL}/${url}`;
    return this.http.get(address);
  }

  getStations(): Observable<Station[]> {
    return new Observable(subscribe => {
      if (this.stations == null || this.stations.length == 0) {
        this.getData('metadata/stations').subscribe((data) => {
          this.stations = data.map(json => {
            let station = Object.create(Station.prototype);
            return Object.assign(station, json, {});
          }).filter(station => {return station.passengerTraffic});
          subscribe.next(this.stations);
          subscribe.complete;
        })
      } else {
        subscribe.next(this.stations);
        subscribe.complete;
      }

    })
  }
  getStation(shortCode: string){
    for (const station of this.stations) {
      if (station.stationShortCode == shortCode){
        return station;
      }
    }
    return null;
  }
}
