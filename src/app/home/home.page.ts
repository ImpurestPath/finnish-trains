import { StationService } from './../station.service';
import { ActivatedRoute } from '@angular/router';

import { TrainServiceService } from './../train-service.service';
import { Component } from '@angular/core';
import { Train } from 'src/train';
import { Station } from 'src/station';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  noTrains = false;
  items: Train[];
  fromStation: Station;
  toStation: Station;
  constructor(private trainServiceService: TrainServiceService,
    private activatedRoute: ActivatedRoute,
    private stationService: StationService) { }

  ngOnInit() {
    const from = this.activatedRoute.snapshot.paramMap.get('from');
    const to = this.activatedRoute.snapshot.paramMap.get('to');
    this.trainServiceService.getTrains(from, to).subscribe((data) => {
    this.items = data;
    if (this.items == null) {
      this.noTrains = true;
      console.log('No trains');

    }
    else {
      this.noTrains = false;
      console.log("have trains");
    }
    });
    console.log(this.items);

    this.fromStation = this.stationService.getStation(from);
    this.toStation = this.stationService.getStation(to);
  }

}
