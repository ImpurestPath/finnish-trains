import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TraindetailsPage } from './traindetails.page';

const routes: Routes = [
  {
    path: '',
    component: TraindetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TraindetailsPageRoutingModule {}
