import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TraindetailsPage } from './traindetails.page';

describe('TraindetailsPage', () => {
  let component: TraindetailsPage;
  let fixture: ComponentFixture<TraindetailsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraindetailsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TraindetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
