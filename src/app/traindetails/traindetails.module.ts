import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TraindetailsPageRoutingModule } from './traindetails-routing.module';

import { TraindetailsPage } from './traindetails.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TraindetailsPageRoutingModule
  ],
  declarations: [TraindetailsPage]
})
export class TraindetailsPageModule {}
