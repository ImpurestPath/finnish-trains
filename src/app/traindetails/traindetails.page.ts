import { TrainServiceService } from './../train-service.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Train } from 'src/train';

@Component({
  selector: 'app-traindetails',
  templateUrl: './traindetails.page.html',
  styleUrls: ['./traindetails.page.scss'],
})
export class TraindetailsPage implements OnInit {
  trainNumber: number;
  train: Train;
  constructor(private activatedRoute: ActivatedRoute, private trainServiceService: TrainServiceService) { }

  ngOnInit() {
    this.trainNumber = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
    this.train = this.trainServiceService.getExistingTrain(this.trainNumber);
  }

}
