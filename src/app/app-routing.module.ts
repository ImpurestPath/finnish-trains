import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'search', pathMatch: 'full' },
  { path: 'home/:from/:to', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'traindetails/:id',
    loadChildren: () => import('./traindetails/traindetails.module').then( m => m.TraindetailsPageModule)
  },
  {
    path: 'search',
    loadChildren: () => import('./search/search.module').then( m => m.SearchPageModule)
  },
  {
    path: 'stationsmap',
    loadChildren: () => import('./stationsmap/stationsmap.module').then( m => m.StationsmapPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
