import { Router } from '@angular/router';
import { StationService } from './../station.service';
import { Component, OnInit } from '@angular/core';
import { Station } from 'src/station';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  stations: Station[];
  stationFrom: Station;
  stationTo: Station;
  date: Date;
  error = false;
  constructor(private stationService: StationService, private router: Router) { }

  ngOnInit() {
    this.stationService.getStations().subscribe(data => this.stations = data);
  }

  onClick() {
    if (this.stationFrom == undefined || this.stationTo == undefined) {
      this.error = true;
    } else {
      this.error = false;
      this.router.navigate(['/home/', this.stationFrom.stationShortCode, this.stationTo.stationShortCode])
    }
  }
  onMap() {
    this.router.navigate(['stationsmap']);
  }
}
