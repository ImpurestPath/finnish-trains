import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StationsmapPage } from './stationsmap.page';

const routes: Routes = [
  {
    path: '',
    component: StationsmapPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StationsmapPageRoutingModule {}
