import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StationsmapPage } from './stationsmap.page';

describe('StationsmapPage', () => {
  let component: StationsmapPage;
  let fixture: ComponentFixture<StationsmapPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StationsmapPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StationsmapPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
