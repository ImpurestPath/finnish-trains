import { StationService } from './../station.service';
import { Component, OnInit } from '@angular/core';
import { Map, tileLayer, marker, icon } from 'leaflet';
import { Station } from 'src/station';

@Component({
  selector: 'app-stationsmap',
  templateUrl: './stationsmap.page.html',
  styleUrls: ['./stationsmap.page.scss'],
})
export class StationsmapPage implements OnInit {

  constructor(private stationService: StationService) { }

  ngOnInit() {
    console.log('oninitmap');

    this.stationService.getStations().subscribe((stations: Station[]) => {
      const map = new Map('map');
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      }).addTo(map);
      const customMarkerIcon = icon({
        iconUrl: '../../assets/icon/marker.svg',
        iconSize: [16, 16],
        popupAnchor: [0, -20]
      });
      stations.forEach(station => {
        marker([station.latitude, station.longitude], { icon: customMarkerIcon })
          .bindPopup(`<b>${station.stationName}</b>`, { autoClose: false })
          // .on('click', () => this.router.navigateByUrl('/restaurant'))
          .addTo(map);
        //.openPopup();
      });

      map.setView([stations[0].latitude, stations[0].longitude], 5);
      setTimeout(() => {
        map.invalidateSize();
      }, 0);

    });

  }
  onClick() {
    console.log('clicked!');
    
  }
}
