import { Train } from 'src/train';

import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TrainStop } from 'src/trainstop';

const API_URL = environment.apiUrl;
@Injectable({
  providedIn: 'root'
})
export class TrainServiceService {
  trains: Train[];
  constructor(private http: HttpClient) { }

  getData(url): Observable<any> {

    const address = `${API_URL}/${url}`;
    console.log(address);
    return this.http.get(address);
  }


  getTrains(from: string, to: string): Observable<Train[]> {
    return new Observable(subscribe => {
      this.getData(`live-trains/station/${from}/${to}`).subscribe((data) => {
        if (data.code == "TRAIN_NOT_FOUND") {
          subscribe.next(null);
          subscribe.complete;
        }
        else {
          this.trains = data.map(json => {
            let train = Object.create(Train.prototype);
            return Object.assign(train, json, {
              timeTableRows: json.timeTableRows.map(row => {
                let trainstop = Object.create(TrainStop.prototype);
                return Object.assign(trainstop, row, { scheduledTime: new Date(row.scheduledTime) });
              })
            });
          });
          subscribe.next(this.trains);
          subscribe.complete;
        }
      })
    })
  }
  getExistingTrains() {
    return this.trains;
  }

  getExistingTrain(id: number): Train {
    for (const train of this.trains) {
      if (train.trainNumber == id) {
        return train;
      }
    }
    return null;
  }
}
