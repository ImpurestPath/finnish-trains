export class TrainStop{
    stationShortCode: string;
    stationUICCode: number;
    countryCode: string;
    type : string;
    trainStopping: boolean;
    commercialTrack: string;
    cancelled: boolean;
    scheduledTime: Date;
    constructor(){}
}