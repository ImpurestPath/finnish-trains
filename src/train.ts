import { TrainStop } from './trainstop';
export class Train{
    trainNumber: number;
    departureDate: string;
    trainType: string;
    trainCategory: string;
    runningCurrently: boolean;
    cancelled: boolean;
    version: number;
    timetableType: string;
    timeTableRows: TrainStop[];
    constructor(){}

}